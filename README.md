# Car List Demo App

Fetches a list of cars from a given Api end point and displays in a scrollable list

- iPhone: one item per row
- iPad portrait: two items per row
- iPad landscape: three items per row

Clicking on the car item will show its detail

![image](https://i.imgur.com/rOUyOWQ.jpg)

## Design

- The app uses a MVVM architectural pattern, which looks like below

![image](https://i.imgur.com/dXIS4dA.png)

- model and view models are unit tested
- The app does not use any third party dependencies
- uses a class called `Box` to bind the view model to the view
- uses a class called `ImageLoader` to implement a simple caching of images

## Requirements

The sample code is tested and runs in below config:

- Swift 5 or above
- Xcode 11.3.1
- iOS 12 or above

### Running

- Testing: Hit `cmd + u` to test the Unit tests.
- Running: Hit `cmd + r` to run on simulator.

> This demo was completed by [Suraj Pathak ](mailto:freesuraj@gmail.com).