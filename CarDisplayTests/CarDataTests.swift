//
//  CarDataTests.swift
//  CarDisplayTests
//
//  Created by Suraj Pathak on 7/3/20.
//  Copyright © 2020 Suraj. All rights reserved.
//

import XCTest
@testable import CarDisplay

class CarDataTests: XCTestCase {
    
    var carData: CarData!

    override func setUp() {
        let data = SampleData.carList.data(using: .utf8, allowLossyConversion: true)!
        carData = try! JSONDecoder().decode(CarListResult.self, from: data).result.first
    }

    override func tearDown() {
        carData = nil
    }

    func testCarDecodesCorrectly() {
        XCTAssertEqual(carData.id, "123")
        XCTAssertEqual(carData.title, "Sample Title")
        XCTAssertEqual(carData.location, "Victoria")
        XCTAssertEqual(carData.price, "$53,081")
        XCTAssertEqual(carData.photo, "https://example.jpg")
        XCTAssertEqual(carData.detailsUrl, "/v2/details")
    }

}
