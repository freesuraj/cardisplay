//
//  CarListViewModelTest.swift
//  CarDisplayTests
//
//  Created by Suraj Pathak on 7/3/20.
//  Copyright © 2020 Suraj. All rights reserved.
//

import XCTest
@testable import CarDisplay

class CarListViewModelTest: XCTestCase {

    override func setUp() {}

    override func tearDown() {}

    func testFetchCarListUpdatesList() {
        let expectation = self.expectation(description: "Fetch car using view model")
        let carListViewModel = CarListViewModel(MockApiService(SampleData.carList))
        carListViewModel.rows.bind { list in
            if list.count == 1 && list.first?.title == "Sample Title" {
                expectation.fulfill()
            }
        }
        
        carListViewModel.fetchCarList()
        waitForExpectations(timeout: 1, handler: nil)
    }

}
