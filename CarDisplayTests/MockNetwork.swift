//
//  MockNetwork.swift
//  CarDisplayTests
//
//  Created by Suraj Pathak on 7/3/20.
//  Copyright © 2020 Suraj. All rights reserved.
//

import Foundation
@testable import CarDisplay

class MockApiService: ApiService {
    
    var mockResponse: String
    init(_ mockResponse: String) {
        self.mockResponse = mockResponse
    }
    
    var baseUrl: URL = URL(string: "example")!
    
    func getCarList(_ completion: @escaping (([CarData]) -> Void)) {
        let data = mockResponse.data(using: .utf8, allowLossyConversion: true)!
        do {
            let decoded = try JSONDecoder().decode(CarListResult.self, from: data)
            completion(decoded.result)
        } catch {
            completion([])
        }
    }
    
    func getCarDetail(path: String, _ completion: @escaping ((CarDetailData?) -> Void)) {
        let data = mockResponse.data(using: .utf8, allowLossyConversion: true)!
        do {
            let decoded = try JSONDecoder().decode(CarDetailData.self, from: data)
            completion(decoded)
        } catch {
            completion(nil)
        }
    }
    
}
