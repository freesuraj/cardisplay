//
//  SampleData.swift
//  CarDisplayTests
//
//  Created by Suraj Pathak on 8/3/20.
//  Copyright © 2020 Suraj. All rights reserved.
//

import XCTest

struct SampleData {
    
    static let carList = """
    {
        "Result": [
            {
              "Id": "123",
              "Title": "Sample Title",
              "Location": "Victoria",
              "Price": "$53,081",
              "MainPhoto": "https://example.jpg",
              "DetailsUrl": "/v2/details"
            }
        ]
    }
    """
    
    static let carDetail = """
           {
             "Id": "123",
             "SaleStatus": "Available",
             "Title": "Sample Title",
             "Overview": {
               "Location": "Victoria",
               "Price": "$53,081",
               "Photos": [
                 "photo1",
                 "photo2"
               ]
             },
             "Comments": "Sample comment"
           }
           """
    
}
