//
//  CarDetailViewModelTest.swift
//  CarDisplayTests
//
//  Created by Suraj Pathak on 8/3/20.
//  Copyright © 2020 Suraj. All rights reserved.
//

import XCTest
@testable import CarDisplay

class CarDetailViewModelTest: XCTestCase {

    override func setUp() {}

    override func tearDown() {}

    func testFetchCarDetailUpdatesCallback() {
        let expectation = self.expectation(description: "Fetch car detail using view model")
        let carDetailViewModel = CarDetailViewModel(MockApiService(SampleData.carDetail))
        carDetailViewModel.didUpdate = {
            if carDetailViewModel.location == "Victoria", carDetailViewModel.saleStatus == "Available" {
                expectation.fulfill()
            }
        }
        carDetailViewModel.getCarDetail(at: "/path")
        waitForExpectations(timeout: 1, handler: nil)
    }

}
