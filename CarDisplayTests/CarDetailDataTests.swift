//
//  CarDetailDataTests.swift
//  CarDisplayTests
//
//  Created by Suraj Pathak on 7/3/20.
//  Copyright © 2020 Suraj. All rights reserved.
//

import XCTest
@testable import CarDisplay

class CarDetailDataTests: XCTestCase {
    
    var carDetailData: CarDetailData!

    override func setUp() {
        let data = SampleData.carDetail.data(using: .utf8, allowLossyConversion: true)!
        carDetailData = try! JSONDecoder().decode(CarDetailData.self, from: data)
    }

    override func tearDown() {
        carDetailData = nil
    }

    func testCarDetailInfoDecodesCorrectly() {
        XCTAssertEqual(carDetailData.id, "123")
        XCTAssertEqual(carDetailData.saleStatus, "Available")
        XCTAssertEqual(carDetailData.title, "Sample Title")
        XCTAssertEqual(carDetailData.comments, "Sample comment")
    }
    
    func testCarOverViewDecodesCorrectly() {
        XCTAssertEqual(carDetailData.overview.location, "Victoria")
        XCTAssertEqual(carDetailData.overview.price, "$53,081")
        XCTAssertEqual(carDetailData.overview.photos, ["photo1", "photo2"])
    }

}
