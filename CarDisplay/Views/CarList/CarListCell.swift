//
//  CarDataCell.swift
//  CarDisplay
//
//  Created by Suraj Pathak on 7/3/20.
//  Copyright © 2020 Suraj. All rights reserved.
//

import UIKit

class CarListCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func customize(with viewModel: CarCellViewModel) {
        titleLabel.text = viewModel.title
        priceLabel.text = viewModel.price
        locationLabel.text = viewModel.location
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        ImageLoader.shared.loadImage(from: viewModel.photo) { [weak self] image in
            self?.imageView.image = image
        }
    }
    
    static var identifier: String {
        return "CarListCell"
    }
}
