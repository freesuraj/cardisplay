//
//  CarListViewController.swift
//  CarDisplay
//
//  Created by Suraj Pathak on 7/3/20.
//  Copyright © 2020 Suraj. All rights reserved.
//

import UIKit

class CarListViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    private let viewModel = CarListViewModel()
    var items: [CarCellViewModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Car List"
        setupCollectionView()
        viewModel.rows.bind { [weak self] rows in
            self?.items = rows
            self?.collectionView.reloadData()
        }
        viewModel.fetchCarList()
    }
    
    func setupCollectionView() {
        collectionView.register(UINib(nibName: CarListCell.identifier, bundle: nil), forCellWithReuseIdentifier: CarListCell.identifier)
        collectionView.dataSource = self
        collectionView.delegate = self
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        collectionView.collectionViewLayout.invalidateLayout()
    }
    
    func openCarDetail(at path: String) {
        guard let detailVc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: CarDetailViewController.identifier) as? CarDetailViewController else { return }
        detailVc.detailPath = path
        navigationController?.pushViewController(detailVc, animated: true)
    }
}

extension CarListViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CarListCell.identifier, for: indexPath)
        if let cell = cell as? CarListCell {
            cell.customize(with: items[indexPath.row])
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout _: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = preferredWidth(constrainedIn: collectionView.frame.width)
        let height = width * 0.75 + 90
        return CGSize(width: width, height: height)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let detailPath = items[indexPath.row].detailPath
        openCarDetail(at: detailPath)
    }
    
    private func preferredWidth(constrainedIn width: CGFloat) -> CGFloat {
        guard isTablet else { return width }
        let grids: CGFloat = isPortrait ? 1:2
        let gridWidth = grids * 5
        return (width-gridWidth)/(grids+1)
    }
}

