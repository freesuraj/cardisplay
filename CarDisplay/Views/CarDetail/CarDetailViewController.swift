//
//  CarDetailViewController.swift
//  CarDisplay
//
//  Created by Suraj Pathak on 8/3/20.
//  Copyright © 2020 Suraj. All rights reserved.
//

import UIKit

class CarDetailViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var verticalStackView: UIStackView!
    
    static let identifier: String = "CarDetailViewController"
    
    private var viewModel = CarDetailViewModel()
    var detailPath: String?
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.didUpdate = setupDetailRows
        if let path = detailPath {
            viewModel.getCarDetail(at: path)
        }
    }
    
    func setupDetailRows() {
        title = viewModel.title
        let allrows = CarDetailSubview.allCases
        verticalStackView.arrangedSubviews.forEach { $0.removeFromSuperview() }
        for row in allrows {
            guard let rowView = row.rowWith(viewModel: viewModel) else { continue }
            verticalStackView.addArrangedSubview(rowView)
            // Adjust the aspect ratio of the photo view
            guard let photoView = rowView as? PhotoScrollView else { continue }
            photoView.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                photoView.widthAnchor.constraint(equalTo: self.view.widthAnchor),
                photoView.heightAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 2/3)
            ])
        }
    }

}
