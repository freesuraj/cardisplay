//
//  CarDetailSubview.swift
//  CarDisplay
//
//  Created by Suraj Pathak on 8/3/20.
//  Copyright © 2020 Suraj. All rights reserved.
//

import UIKit

enum CarDetailSubview: CaseIterable {
    case banner, location, status, price, comments
    
    func rowWith(viewModel: CarDetailViewModel) -> UIView? {
        switch self {
        case .banner: return PhotoScrollView.scrollView(with: viewModel.photos)
        case .location: return Self.label(with: viewModel.location)
        case .status: return Self.label(with: viewModel.saleStatus)
        case .price: return Self.label(with: viewModel.price)
        case .comments: return Self.label(with: viewModel.comments)
        }
    }
    
    static func label(with text: String) -> UIView {
        let label = UILabel()
        label.textAlignment = .left
        label.numberOfLines = 0
        label.text = text
        let labelHolder = UIStackView(arrangedSubviews: [label])
        labelHolder.alignment = .leading
        labelHolder.axis = .horizontal
        labelHolder.distribution = .fill
        labelHolder.spacing = 0
        labelHolder.layoutMargins = .init(top: 0, left: 20, bottom: 0, right: 20)
        labelHolder.isLayoutMarginsRelativeArrangement = true
        return labelHolder
    }
}
