//
//  PhotoScrollView.swift
//  CarDisplay
//
//  Created by Suraj Pathak on 8/3/20.
//  Copyright © 2020 Suraj. All rights reserved.
//

import UIKit

class PhotoScrollView: UIView, UIScrollViewDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIStackView!
    @IBOutlet weak var pageControl: UIPageControl!

    private func setupImages(_ images: [String]) {
        scrollView.isPagingEnabled = true
        pageControl.numberOfPages = images.count
        scrollView.delegate = self
        for item in images {
            let imageView = UIImageView()
            imageView.isUserInteractionEnabled = true
            imageView.contentMode = .scaleAspectFit
            ImageLoader.shared.loadImage(from: item) { image in
                imageView.image = image
            }
            contentView.addArrangedSubview(imageView)
            imageView.clipsToBounds = true
            imageView.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                imageView.widthAnchor.constraint(equalTo: scrollView.widthAnchor),
                imageView.heightAnchor.constraint(equalTo: scrollView.heightAnchor)
            ])
        }
        bringSubviewToFront(pageControl)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        pageControl.currentPage = Int((scrollView.contentOffset.x + (0.5 * scrollView.frame.size.width)) / scrollView.frame.width)
    }
    
    static func scrollView(with images: [String]) -> PhotoScrollView? {
        guard images.count > 0, let view = UINib.init(nibName: "PhotoScrollView", bundle: nil).instantiate(withOwner: nil, options: nil).first as? PhotoScrollView else { return nil }
        view.setupImages(images)
        return view
    }
}
