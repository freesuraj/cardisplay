//
//  CarData.swift
//  CarDisplay
//
//  Created by Suraj Pathak on 7/3/20.
//  Copyright © 2020 Suraj. All rights reserved.
//

import Foundation

struct CarListResult: Decodable {
    let result: [CarData]

    enum CodingKeys: String, CodingKey {
        case result = "Result"
    }
}

struct CarData: Decodable {
    let id: String
    let title: String
    let price: String?
    let photo: String
    let location: String?
    let detailsUrl: String
    
    enum CodingKeys: String, CodingKey {
        case id = "Id"
        case title = "Title"
        case price = "Price"
        case photo = "MainPhoto"
        case location = "Location"
        case detailsUrl = "DetailsUrl"
    }
}
