//
//  CarDetailData.swift
//  CarDisplay
//
//  Created by Suraj Pathak on 7/3/20.
//  Copyright © 2020 Suraj. All rights reserved.
//

import Foundation

// MARK: - CarDetailData
struct CarDetailData: Decodable {
    let id, saleStatus, title: String
    let overview: Overview
    let comments: String

    enum CodingKeys: String, CodingKey {
        case id = "Id"
        case saleStatus = "SaleStatus"
        case title = "Title"
        case overview = "Overview"
        case comments = "Comments"
    }
}

// MARK: - Overview
struct Overview: Decodable {
    let location, price: String
    let photos: [String]

    enum CodingKeys: String, CodingKey {
        case location = "Location"
        case price = "Price"
        case photos = "Photos"
    }
}
