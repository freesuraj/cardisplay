//
//  ImageLoader.swift
//  CarDisplay
//
//  Created by Suraj Pathak on 7/3/20.
//  Copyright © 2020 Suraj. All rights reserved.
//

import UIKit

/// A quick image loader from a url string, with a simple caching mechanism
class ImageLoader {
    typealias ImageBlock = ((UIImage?) -> Void)
    static let shared = ImageLoader()
    private var cachedImages: [String: UIImage] = [:]
    
    private init() {}
    
    /// Loads image from a url string asynchronously. It caches the image temporarily in a cache
    func loadImage(from urlString: String, _ completion: @escaping ImageBlock) {
        if let cached = cachedImages[urlString] {
            completion(cached)
            return
        }
        
        DispatchQueue(label: "com.image.suraj").async {
            guard let url = URL(string: urlString),
                let data = try? Data(contentsOf: url) else {
                    DispatchQueue.main.async { completion(nil) }
                    return
            }
            let image = UIImage(data: data)
            if let validImage = image {
                DispatchQueue.main.async { self.cachedImages[urlString] = validImage }
            }
            DispatchQueue.main.async { completion(image) }
        }
    }
    
}
