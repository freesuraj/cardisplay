//
//  UIView+Screen.swift
//  CarDisplay
//
//  Created by Suraj Pathak on 7/3/20.
//  Copyright © 2020 Suraj. All rights reserved.
//

import UIKit

let isTablet: Bool = UIDevice.current.userInterfaceIdiom == .pad
var isPortrait: Bool { return UIScreen.main.bounds.width < UIScreen.main.bounds.height }
