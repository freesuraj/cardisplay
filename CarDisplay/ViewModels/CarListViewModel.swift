//
//  CarListViewModel.swift
//  CarDisplay
//
//  Created by Suraj Pathak on 7/3/20.
//  Copyright © 2020 Suraj. All rights reserved.
//

import Foundation

class CarCellViewModel {
    var photo: String
    var title: String
    var price: String?
    var location: String?
    var detailPath: String
    
    init() {
        title = ""
        photo = ""
        detailPath = ""
    }
}

class CarListViewModel {
    var apiService: ApiService?
    var rows: Box<[CarCellViewModel]>
    init(_ apiService: ApiService? = CarApiService()) {
        self.apiService = apiService
        rows = Box([])
    }
    
}

// MARK: - Convenience initializers
extension CarCellViewModel {
    
    convenience init(_ carData: CarData) {
        self.init()
        self.title = carData.title
        self.price = carData.price
        self.location = carData.location
        self.photo = carData.photo
        self.detailPath = carData.detailsUrl
    }
}

extension CarListViewModel {
    
    func fetchCarList() {
        apiService?.getCarList { list in
            self.rows.value = list.compactMap { CarCellViewModel($0) }
        }
    }
}
