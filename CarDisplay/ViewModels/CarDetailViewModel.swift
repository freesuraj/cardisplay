//
//  CarDetailViewModel.swift
//  CarDisplay
//
//  Created by Suraj Pathak on 7/3/20.
//  Copyright © 2020 Suraj. All rights reserved.
//

import Foundation

class CarDetailViewModel {
    var title: String
    var photos: [String]
    var saleStatus: String
    var price: String
    var location: String
    var comments: String
    
    var apiService: ApiService?
    var didUpdate: (() -> Void)?
    
    init(_ apiService: ApiService? = CarApiService()) {
        self.apiService = apiService
        self.photos = []
        self.title = ""
        self.saleStatus = ""
        self.price = ""
        self.location = ""
        self.comments = ""
    }
    
    func getCarDetail(at path: String) {
        apiService?.getCarDetail(path: path, { detail in
            guard let detail = detail else { return }
            self.title = detail.title
            self.photos = detail.overview.photos
            self.photos = detail.overview.photos
            self.saleStatus = detail.saleStatus
            self.price = detail.overview.price
            self.location = detail.overview.location
            self.comments = detail.comments
            self.didUpdate?()
        })
    }
}
