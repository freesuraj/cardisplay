//
//  NetworkService.swift
//  CarDisplay
//
//  Created by Suraj Pathak on 7/3/20.
//  Copyright © 2020 Suraj. All rights reserved.
//

import Foundation

enum NetworkError: Error {
  case invalidResponse
  case noData
  case failedRequest
  case invalidData
}

protocol ApiService {
    /// Base Url
    var baseUrl: URL { get }
    func getCarList(_ completion: @escaping (([CarData]) -> Void))
    func getCarDetail(path: String, _ completion: @escaping ((CarDetailData?) -> Void))
}

extension ApiService {
    
    /// Normal Get request
    func get<T: Decodable>(_ path: String, _ completion: @escaping ((Result<T, NetworkError>) -> Void)) {
        let fullUrl = baseUrl.appendingPathComponent(path)
        let urlRequest = URLRequest(url: fullUrl, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 30)
        URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
          DispatchQueue.main.async {
            guard error == nil else {
                completion(.failure(.failedRequest))
              return
            }
            
            guard let data = data else {
                completion(.failure(.noData))
              return
            }
            
            guard let response = response as? HTTPURLResponse else {
                completion(.failure(.invalidResponse))
              return
            }
            
            guard response.statusCode == 200 else {
                completion(.failure(.failedRequest))
              return
            }
            
            do {
              let decoder = JSONDecoder()
              let decodedData: T = try decoder.decode(T.self, from: data)
                completion(.success(decodedData))
            } catch {
                completion(.failure(.invalidData))
            }
          }
        }.resume()
    }
}
