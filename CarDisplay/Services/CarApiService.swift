//
//  CarDataService.swift
//  CarDisplay
//
//  Created by Suraj Pathak on 7/3/20.
//  Copyright © 2020 Suraj. All rights reserved.
//

import Foundation

fileprivate struct CarApiServiceConstants {
    static let baseUrl = "https://www.mocky.io"
    static let carListPath = "/v2/5e61e29230000064004d55a6"
}

class CarApiService: ApiService {
    
    let baseUrl: URL
    private let carListPath: String
    
    init?(baseUrlString: String = CarApiServiceConstants.baseUrl, listPath: String = CarApiServiceConstants.carListPath) {
        guard let url = URL(string: baseUrlString) else {
            print("could not initialize car data service. base url is incorrect")
            return nil
        }
        self.baseUrl = url
        self.carListPath = listPath
    }
    
    func getCarList(_ completion: @escaping (([CarData]) -> Void)) {
        get(carListPath) { (result: Result<CarListResult, NetworkError>) in
            switch result {
            case .success(let list):
                completion(list.result)
            case .failure(let error):
                print("error in getting car list \(error)")
                completion([])
            }
        }
    }
    
    func getCarDetail(path: String, _ completion: @escaping ((CarDetailData?) -> Void)) {
        get(path) { (result: Result<CarDetailData, NetworkError>) in
            switch result {
            case .success(let data):
                completion(data)
            case .failure(let error):
                print("error in getting car detail \(error)")
                completion(nil)
            }
        }
    }
}
